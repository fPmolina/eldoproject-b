from django.contrib import admin
#from .models import Producto, Categoria
from .models import Articulo, Seccion

# Register your models here.
admin.site.register(Articulo)
admin.site.register(Seccion)
#admin.site.register(Producto)
#admin.site.register(Categoria)