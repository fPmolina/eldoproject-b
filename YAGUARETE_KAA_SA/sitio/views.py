from django.contrib.auth.models import User
from django.http.response import HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404
from django.urls import reverse
from .forms import *
#from .models import Categoria, Producto
from .models import Articulo, Seccion
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.auth.decorators import permission_required  
from eCommerce import urls
from sitio import urls
# from django.contrib.auth.decorators import permission_required

# Create your views here.

def index(request):
    if "leer_mas_tarde" not in request.session:
        request.session["leer_mas_tarde"] = []
    return render(request,"eCommerce/index.html", {
        "lista_articulos": Articulo.objects.all(),
        "lista_secciones": Seccion.objects.all(),
        "leer_mas_tarde": request.session["leer_mas_tarde"],
    })
#_____________________________________________________________
#def index(request):
#    if "ver_mas_tarde" not in request.session:
#        request.session["ver_mas_tarde"] = []
#    return render(request,"eCommerce/index.html", {
#        "lista_productos": Producto.objects.all(),
#        "lista_categorias": Categoria.objects.all(),
#        "ver_mas_tarde": request.session["ver_mas_tarde"],
#    })


def pantallaDeAcercaDe(request):
    return render(request,"pantallaDeAcercaDe.html")


        
def carrito(request):
    return render(request,"carrito.html")

def registro(request):
    return render(request, "eCommerce/registro.html")

def articulo(request, articulo_id):
    un_articulo = get_object_or_404(Articulo, id=articulo_id)
    return render(request, "eCommerce/articulo.html", {
        "articulo": un_articulo
    })
#_____________________________________________________________
#def producto(request, producto_id):
#    un_producto = get_object_or_404(Producto, id=producto_id)
#    return render(request, "eCommerce/producto.html", {
#        "producto": un_producto
#    })

def articulo_alta(request):
    if request.method == "POST":
        user = User.objects.get(username=request.user)
        form = FormArticulo(request.POST, request.FILES, instance=Articulo(imagen=request.FILES['imagen'], publicador=user))      
        if form.is_valid():
            form.save()
            return redirect("sitio:index")          
    else:
        form = FormArticulo(initial={'fecha_publicacion':timezone.now()})
        return render(request, "eCommerce/articulo_nuevo.html", {
            "form": form
        })
#_____________________________________________________________
#def producto_alta(request):
#    if request.method == "POST":
#        user = User.objects.get(username=request.user)
#        form = FormProducto(request.POST, request.FILES, instance=Producto(imagen=request.FILES['imagen'], publicador=user))      
#        if form.is_valid():
#            form.save()
#            return redirect("sitio:index")          
#    else:
#        form = FormProducto(initial={'fecha_registro':timezone.now()})
#        return render(request, "eCommerce/producto_nuevo.html", {
#            "form": form
#        })


def articulo_editar(request, articulo_id):
    un_articulo = get_object_or_404(Articulo, id=articulo_id)
    if request.method == "POST":  
        user = User.objects.get(username=request.user)   
        un_articulo.publicador = user
        form = FormArticulo(data=request.POST, files=request.FILES, instance=un_articulo)
        if form.is_valid():
            form.save()
            return redirect("sitio:index")
    else:
        form = FormArticulo(instance = un_articulo)
        return render(request, 'eCommerce/articulo_edicion.html', {
            "articulo": un_articulo,
            "form": form
        })
#_________________________________________________________________
#def producto_editar(request, producto_id):
#    un_producto = get_object_or_404(Producto, id=producto_id)
#    if request.method == "POST":  
#        user = User.objects.get(username=request.user)   
#        un_producto.moderador = user
#        form = FormProducto(data=request.POST, files=request.FILES, instance=un_producto)
#        if form.is_valid():
#            form.save()
#            return redirect("sitio:index")
#    else:
#        form = FormProducto(instance = un_producto)
#        return render(request, 'eCommerce/producto_edicion.html', {
#            "producto": un_producto,
#            "form": form
#       })



def articulo_eliminar(request, articulo_id):
    un_articulo = get_object_or_404(Articulo, id=articulo_id)
    un_articulo.delete()
    return redirect("sitio:index")
#_________________________________________________________________
#def producto_eliminar(request, articulo_id):
#    un_producto = get_object_or_404(Producto, id=articulo_id)
#    un_producto.delete()
#    return redirect("sitio:index")

def filtro_secciones(request, seccion_id):
    una_seccion = get_object_or_404(Seccion, id=seccion_id)
    queryset = Articulo.objects.all()
    queryset = queryset.filter(seccion=una_seccion)
    return render(request,"eCommerce/index.html", {
        "lista_articulos": queryset,
        "lista_secciones": Seccion.objects.all(),
        "seccion_seleccionada": una_seccion
    })

def filtro_articulos(request, articulo_id):
     if request.method == "POST":  
        return print(articulo_id)

def resultadoBusqueda(request):
    return render(request,"resultadoDeBusqueda.html")

#_________________________________________________________________
#def filtro_categorias(request, seccion_id):
#    una_categoria = get_object_or_404(Categoria, id=seccion_id)
#    queryset = Categoria.objects.all()
#    queryset = queryset.filter(categoria=una_categoria)
#    return render(request,"eCommerce/index.html", {
#        "lista_productos": queryset,
#        "lista_categorias": Categoria.objects.all(),
#        "categoria_seleccionada": una_categoria
#    })



@login_required
def leer_mas_tarde(request, articulo_id):
    un_articulo = get_object_or_404(Articulo, id=articulo_id)
    for id in request.session["leer_mas_tarde"]:
        if id == articulo_id:
            #existe el articulo
            return HttpResponseRedirect(reverse("sitio:articulo", args=(un_articulo.id,)))            
    request.session["leer_mas_tarde"] += [articulo_id]
    return HttpResponseRedirect(reverse("sitio:articulo", args=(un_articulo.id,)))   



#___________________________________________________________________________________________________
#Producto

#@login_required
#def ver_mas_tarde(request, producto_id):
#    un_producto = get_object_or_404(Producto, id=producto_id)
#    for id in request.session["ver_mas_tarde"]:
#        if id == producto_id:
#            #existe el producto
#            return HttpResponseRedirect(reverse("sitio:producto", args=(un_producto.id,)))            
#    request.session["ver_mas_tarde"] += [producto_id]
#    return HttpResponseRedirect(reverse("sitio:producto", args=(un_producto.id,)))   


