from django.urls import path, include
from . import views
from eCommerce.urls import path, include




app_name = "sitio"
urlpatterns = [
    path('', views.index, name="index"),
    
    path('filtro_secciones/<int:seccion_id>', views.filtro_secciones, name="filtro_secciones"),
    path('<int:articulo_id>', views.articulo, name="articulo"),
    path('articulo_alta', views.articulo_alta, name="articulo_alta"),
    path('articulo_editar/<int:articulo_id>', views.articulo_editar, name="articulo_editar"),
    path('articulo_eliminar/<int:articulo_id>', views.articulo_eliminar, name="articulo_eliminar"),
    path('leer_mas_tarde/<int:articulo_id>', views.leer_mas_tarde, name="leer_mas_tarde"),
    
    path('pantallaDeAcercaDe', views.pantallaDeAcercaDe, name="pantallaDeAcercaDe"),
    path('carrito', views.carrito, name="carrito"),
    path('resultadoBusqueda', views.resultadoBusqueda, name="resultadoDeBusqueda"),
    path('registro.html', views.registro, name="registro"),
    
    #path('filtro_categorias/<int:categoria_id>', views.filtro_categorias, name="filtro_categorias"),
    #path('<int:producto_id>', views.producto, name="producto"),
    #path('producto_alta', views.producto_alta, name="producto_alta"),
    #path('producto_editar/<int:producto_id>', views.producto_editar, name="producto_editar"),
    #path('producto_eliminar/<int:producto_id>', views.producto_eliminar, name="producto_eliminar"),
    #path('ver_mas_tarde/<int:producto_id>', views.ver_mas_tarde, name="ver_mas_tarde"),
]