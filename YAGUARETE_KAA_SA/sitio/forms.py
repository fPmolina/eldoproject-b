from django import forms
#from .models import Producto
from .models import Articulo

class FormArticulo(forms.ModelForm):

    #campos del modelo
    class Meta:
        model = Articulo
        fields = ('seccion', 'fecha_publicacion', 'titulo', 'contenido', 'imagen')
        widgets = {
            'fecha_publicacion': forms.SelectDateWidget(attrs={'class': 'pub_fecha_publicacion'}),
            'titulo': forms.TextInput(attrs={'class': 'pub_titulo'}),
            'contenido': forms.Textarea(attrs={'class': 'pub_contenido'}),
            'imagen': forms.FileInput(attrs={'name':'imagen_adjunta', 'class': 'pub_imagen'}),
            
        }
#___________________________________________________________________________________________________
#Producto
#class FormProducto(forms.ModelForm):
#
#    #campos del modelo
#    class Meta:
#        model = Producto
#        fields = ('categoria', 'precio', 'nombre', 'descripcion', 'imagen')
#        widgets = {
#            'precio': forms.TextInput(attrs={'class': 'pub_precio'}),
#            'nombre': forms.TextInput(attrs={'class': 'pub_nombre'}),
#            'descripcion': forms.Textarea(attrs={'class': 'pub_descripcion'}),
#            'imagen': forms.FileInput(attrs={'name':'imagen_adjunta', 'class': 'pub_imagen'}),
#        }